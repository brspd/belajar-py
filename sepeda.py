class Sepeda(object):
    def milik(self, pemilik):
        if pemilik is "keluarga":
            self.milikBersama()
        else:
            self.milikUmum()

    def milikBersama(self):
        print("milik bersama")

    def milikUmum(self):
        print("milik umum")

sepeda = Sepeda()
sepeda.milik("emh")
sepeda.milik("keluarga")
